// позволяет расширять обьекті методами и свойствами. Когда прототип наследуется ему передаются св-ва и методы прото. Данное свойство можно применять в повторяющихся обьектах

class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	set name(name) {
		return (this._name = name);
	}

	get name() {
		return this._name;
	}

	set age(age) {
		return (this._age = age);
	}

	get age() {
		return this._age;
	}

	set salary(salary) {
		return (this._salary = salary);
	}

	get salary() {
		return this._salary;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this._lang = lang;
	}
	set salary(salary) {
		return (this._salary = salary * 3);
	}

	get salary() {
		return this._salary * 3;
	}
}

const tolik = new Programmer("Tolik", 15, 200, ["Ukr, Eng, Rus"]);
const bob = new Programmer("Bob", 20, 400, "Ukr, Rus");
console.log(tolik);
console.log(bob);
